package com.example;

import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Grid {
    GameObj[][] grid;
    private ArrayList<int[]> free_posions = new ArrayList();
    private Random rand = new Random();

    public Grid(int width, int height) {
        
        // creates an empty grid of only sea types
        grid = new GameObj[height][width];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                grid[i][j] = new Sea();
                int[] pos = { i, j };
                free_posions.add(pos);
            }
        }
    }

    public void addGameObjRandomly(GameObj obj) throws Exception {
        if (free_posions.isEmpty()) {
            throw new Exception("No more free spaces to add object to grid.IE: the grid is full");
        }
        int[] random_free_pos = this.free_posions.remove(this.rand.nextInt(0, this.free_posions.size()));
        this.grid[random_free_pos[0]][random_free_pos[1]] = obj;

    }

    public static void main(String[] args) throws Exception {
        Grid g = new Grid(5, 4);
        g.Print();
        State s = new State();
        s.x_pos = 1;
        s.y_pos = 1;
        s.capacity = 10;
        s.ships.add(new ShipBody(0, 0));
        s.stations.add(new Station(2, 2));
        g = new Grid(s, 3, 3);
        g.Print();
    }

    public Grid(State state, int width, int height) {
        // This funciton takes a state and maps it to a grid to allow from prints and so

        this(width, height);

        // add agent postion

        Guard agent = new Guard();
        agent.x_pos = state.x_pos;
        agent.y_pos = state.y_pos;
        agent.capacity = state.capacity;

        grid[state.y_pos][state.x_pos] = agent;

        // add each ship
        for (ShipBody ship : state.ships) {
            grid[ship.y_pos][ship.x_pos] = ship;
        }

        for (ShipBody wreck : state.wrecks) {
            grid[wreck.y_pos][wreck.x_pos] = wreck;
        }

        for (Station station : state.stations) {
            grid[station.y_pos][station.x_pos] = station;
        }

    }

    public void Print() {
        int max_width_per_element_printed = 10;
        String formater = "%" + max_width_per_element_printed + "s'";
        String str = "";
        String[] arr = { "batee5", "toot", "l", "ronaldooo" };
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {

                System.out.printf(formater, grid[i][j]);
            }
            System.out.println("");
        }
        System.out.println("--------------------------------------------------");
    }

}
