package com.example;

import java.util.ArrayList;
import java.util.Arrays;

public class State {
    // Class representing the current state of the game
    int x_pos;
    int y_pos;
    int capacity;
    ArrayList<ShipBody> ships = new ArrayList<>();
    ArrayList<ShipBody> wrecks = new ArrayList<>();
    ArrayList<Station> stations = new ArrayList<>();
   
    public State(int _x_pos, int _y_pos, int _capacity, ArrayList<ShipBody> _ships, ArrayList<ShipBody> _wrecks, ArrayList<Station> _stations){
        x_pos = _x_pos;
        y_pos = _y_pos;
        capacity = _capacity;
        ships = _ships;
        wrecks = _wrecks;
        stations = _stations; 
    }
    public State(){}
    public static void main(String[] args) {
        State s = new State();
        s.x_pos = 1;
        s.y_pos = 1;
        s.capacity = 10;
        // s.ships = new ArrayList<ShipBody>(Arrays.asList(new ShipBody(), new ShipBody()));
        // s.stations = new ArrayList<Station>(Arrays.asList(new Station(), new Station());

    }
}
