package com.example;

import java.util.ArrayList;

public abstract class SearchAlgorithm {
    abstract Node search(ArrayList<Node> leave_nodes_at_the_moment);
    abstract int searchIndex(ArrayList<Node> leave_nodes_at_the_moment);
}
