package com.example;

import java.util.ArrayList;
import java.util.OptionalInt;

public class BredthFirstSearch extends SearchAlgorithm{

    @Override
    Node search(ArrayList<Node> leave_nodes_at_the_moment) {

        ArrayList<Node> nodes = leave_nodes_at_the_moment;

        // In bredth 1st search we simply take the more shallow node 1st 
        // nodes with less depth are all equal and we must finish them before starting other nodes 
        // so sort noeds via depth and simply choose the 1st one to expand 
       
        Node least_depth_node =  nodes.stream().min((a, b)-> a.depth - b.depth).get();
        
        return least_depth_node;
    }

    @Override
    int searchIndex(ArrayList<Node> leave_nodes_at_the_moment) {
        Node choosen = this.search(leave_nodes_at_the_moment);
        return leave_nodes_at_the_moment.indexOf(choosen);
    }
    
    public static void main(String[] args) {
        /* build a tree like this 
         *          a
         *    b         c
         *  f   t          d 
         */
        Node a = new Node(null, null, null);
        Node b = new Node(null, a, null);
        Node c = new Node(null, a, null);
        Node d = new Node(null, c, null);
        Node f = new Node(null, b, null);
        Node t = new Node(null, b, null);
         
        // Here we try to simulate that a tree is expanded
        ArrayList<Node> leaves = new ArrayList<>();
        leaves.add(a);

        SearchAlgorithm s = new BredthFirstSearch();
        Node r = s.search(leaves); 
        System.out.println(r==a);
        // now we remove a and addd b and c 

        SearchTree st = new SearchTree(a);
        int i =0;
        while(i<5000){
           System.out.println(st.leaves_nodes.size());
           System.out.println("------------------------------------------------");
           int indx = s.searchIndex(st.leaves_nodes);
           ArrayList<Node> new_nodes = st._fakeExpandatIndex(indx);
           st.leaves_nodes.addAll(new_nodes); // add them to the nodes
           i++;
        }
    }
}
