package com.example;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.rits.cloning.Cloner;
public class SearchTree {
    Node root;
    public ArrayList<Node> leaves_nodes = new ArrayList<>();
    public SearchTree(Node starting_node){
        root = starting_node;
        leaves_nodes.add(root);
    }
    public void expandNode(Node n){
        // TODO: expand nodes basd on the availible actions and add ll of the new nodes to the leaves tree
    }
    public ArrayList<Node> expandNodeIndex(int indx){
        Node n = this.leaves_nodes.remove(indx);
        // to expand this node we 1 get all availible actions 
        ArrayList<Action> availible_actions = n.getAvailaibleActions();
        ArrayList<Node> expanded_nodes = new ArrayList<>();
        for (Action action : availible_actions) {
            if(action==Action.Move_up){
                // add extra node to tree
                // node is the node after we expanded the tree
                Node node = n.deepClone();
                // Now we alter node based on what happened 
                node.state.y_pos = node.state.y_pos-1; // cuz we moved up // ? also we checked wheather this is availible action already
                // Add this action as the prv_action
                // Add this node to the list of expanded nodes 
                expanded_nodes.add(node);
            }
            // TODO: DO for the rest of the actions 
        }

        // now we calculate properties for all the new expanded nodes
        for (Node node : expanded_nodes) {
            node.prv_node = n; // make this n the parent node
            // TODO: Calculate herustic for this node 
            node.heurestic = 0;
            // path cost here is time. so increas by 1 cuz we moved 1 step
            node.path_cost = 1+n.path_cost;
            node.depth = 1+n.depth;
        }
        return expanded_nodes;
    }
    public ArrayList<Node> _fakeExpandatIndex(int indx){
         Node n = this.leaves_nodes.remove(indx);
        // to expand this node we 1 get all availible actions 
        ArrayList<Action> availible_actions = n.getAvailaibleActions();
        ArrayList<Node> expanded_nodes = new ArrayList<>();
        for (Action action : availible_actions) {
                // add extra node to tree
                // node is the node after we expanded the tree
                Node node = n.deepClone();
                // Now we alter node based on what happened 
                // node.state.y_pos = node.state.y_pos-1; // cuz we moved up // ? also we checked wheather this is availible action already
                // Add this action as the prv_action
                // Add this node to the list of expanded nodes 
                expanded_nodes.add(node);
        }

        // now we calculate properties for all the new expanded nodes
        for (Node node : expanded_nodes) {
            node.prv_node = n; // make this n the parent node
            // TODO: Calculate herustic for this node 
            node.heurestic = 0;
            // path cost here is time. so increas by 1 cuz we moved 1 step
            node.path_cost = 1+n.path_cost;
            node.depth = 1+n.depth;
        }
        return expanded_nodes;
       
    }
    public void checkGoal(Node n){
        // TODO: check if node n is goal nsode
    }
    public static void main(String[] args) {
        State inital_State ;
        int x = 0;
        int y = 1;
        int capacity = 20;
        ArrayList<ShipBody> ships = new ArrayList<>();
        ships.add(new ShipBody(0, 2));
        ArrayList<Station> stations = new ArrayList<>();
        stations.add(new Station(0, 0));
        inital_State = new State(x, y, capacity, ships, new ArrayList<ShipBody>(), stations);

        Node initial_node = new Node(inital_State, null, null);
        SearchTree s = new SearchTree(initial_node);

        // simulate an algorithm 

        ArrayList<Node> avaliable_nodes = s.leaves_nodes;
        // just choose the 1st node 
        Node choosen = s.leaves_nodes.get(0);

        // Node clone = choosen.deepClone();
        // // clone is a deep-clone of o
        // // this is the easy part that the 2 nodes are different references 
        // // as long as prints are diffrerent this is deep cloning
        // System.out.println(choosen);
        // choosen.state.x_pos = 5;
        // choosen.state.ships.add(new ShipBody(3, 3));
        // choosen.state.ships.get(0).x_pos = 100;
        // System.out.println(choosen.state.x_pos);
        // System.out.println(clone.state.x_pos);
        // System.out.println(choosen.state.ships.size());
        // System.out.println(clone.state.ships.size());
        // System.out.println(choosen.state.ships.get(0).x_pos);
        // System.out.println(clone.state.ships.get(0).x_pos);

        ArrayList<Node> new_nodes = s.expandNodeIndex(0);
        // After getting all the new nodes and setting them up 
        // we now add the new nodes to current leave nodes so that the search algorithm can choose from them later on

        // ? Notice that if there are no availible actions then we simply won't add any new nodes
        // ? this the number of choices of this branch will disapera and cuz nothing is pointingg at the expanded node 
        // ? as it's children do not exist thus no prv node reference. it will be droped 
        s.leaves_nodes.addAll(new_nodes); // add them all to the pool of nodes the algorithm makes sure of

    }
}
