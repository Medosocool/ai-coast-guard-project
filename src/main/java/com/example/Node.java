package com.example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rits.cloning.Cloner;

import java.util.ArrayList;

public class Node {
   State state;
   Node prv_node;
   Action prv_action;
   int path_cost;
   float heurestic;
   int depth = 0;

   static int gird_width = 10;
   static int grid_height = 10;

   private static final Cloner cloner = new Cloner();
   public Node(State _state, Node _prv_node, Action _prv_action) {
      state = _state;
      prv_node = _prv_node;
      prv_action = _prv_action;
   }

   public ArrayList<Action> getAvailaibleActions() {
      // ! TODO: I now return all actions to test preformance..... MUST CHANGE THIS
      ArrayList<Action> a = new ArrayList<>();
      a.add(Action.Move_up);
      a.add(Action.Move_down);
      a.add(Action.Move_left);
      a.add(Action.Move_right);
      a.add(Action.Pick_up);
      return a;
   }

   public Node deepClone() {
      Node clone=cloner.deepClone(this);
      // clone is a deep-clone of o
      return clone;
   }
   public static void main(String[] args) {
     // Testing deep cloning 
      
   }
   // Helpers
   public int getDepth(){
      return this.depth;
   }
   @Override
   public String toString() {
      return "Node(" + this.depth +")";
   }
}
